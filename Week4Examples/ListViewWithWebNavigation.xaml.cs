﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Week4Examples.Models;
using Xamarin.Forms;

namespace Week4Examples
{
    public partial class ListViewWithWebNavigation : ContentPage
    {
        public ListViewWithWebNavigation()
        {
            InitializeComponent();

            PopulateListView();
        }

        void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var listView = (ListView)sender;
            WebsiteItem itemTapped = (WebsiteItem)listView.SelectedItem;
            var uri = new Uri(itemTapped.url);
            Device.OpenUri(uri);
        }

        private void PopulateListView()
        {
            var listOfSites = new ObservableCollection<WebsiteItem>()
            {
                new WebsiteItem() { websiteName = "Twitter", url = "http://www.twitter.com"},
                new WebsiteItem() { websiteName = "Google", url = "http://www.google.com"},
            };

            WebsiteListView.ItemsSource = listOfSites;
        }
    }
}
